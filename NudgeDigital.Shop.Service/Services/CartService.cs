﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class CartService : ICartService
    {
        private readonly DbContext _dbContext;
        private readonly IMediaService _mediaService;

        public CartService(IDatabaseContextFactory databaseContextFactory, IMediaService mediaService)
        {
            _dbContext = databaseContextFactory.GetDatabaseContext();
            _mediaService = mediaService;
        }

        public IQueryable<Cart> Get() => _dbContext.Set<Cart>();

        public async Task<Cart> GetActiveCart(string customerId, string createdById)
            => await _dbContext.Set<Cart>()
                .Include(x => x.Items)
                .Where(x => x.CustomerId == customerId && x.CreatedBy == createdById && x.IsActive).FirstOrDefaultAsync();

        public async Task<Result> AddToCart(string customerId, string createdById, string productId, int quantity)
        {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null)
            {
                cart = new Cart
                {
                    CustomerId = customerId,
                    CreatedBy = createdById,
                    Id = Guid.NewGuid().ToString(),
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = createdById,
                    IsActive = true
                };

                await _dbContext.Set<Cart>().AddAsync(cart);
            }
            else
            {
                if (cart.LockedOnCheckout)
                    return Result.Fail("Cart is being locked for checkout. Please complete the checkout first");

                cart = await _dbContext.Set<Cart>().Include(x => x.Items).FirstOrDefaultAsync(x => x.Id == cart.Id);
            }

            var cartItem = cart.Items.FirstOrDefault(x => x.ProductId == productId);
            if (cartItem == null)
            {
                cartItem = new CartItem
                {
                    Cart = cart,
                    ProductId = productId,
                    Quantity = quantity,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid().ToString(),
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = createdById,
                    CreatedBy = createdById
                };

                cart.Items.Add(cartItem);
            }
            else
                cartItem.Quantity = cartItem.Quantity + quantity;

            await _dbContext.SaveChangesAsync();

            return Result.Ok();
        }

        public async Task<CartVm> GetActiveCartDetails(string customerId, string createdById)
        {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null)
            {
                return null;
            }

            var cartVm = new CartVm()
            {
                Id = cart.Id,
                ShippingAmount = cart.ShippingAmount,
                OrderNote = cart.OrderNote
            };

            cartVm.Items = _dbContext.Set<CartItem>()
                .Include(x => x.Product).ThenInclude(p => p.ThumbnailImage)
                .Where(x => x.CartId == cart.Id)
                .Select(x => new CartItemVm
                {
                    Id = x.Id,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProductPrice = x.Product.Price,
                    ProductStockQuantity = x.Product.StockQuantity,
                    ProductStockTrackingIsEnabled = x.Product.StockTrackingIsEnabled,
                    IsProductAvailabeToOrder = x.Product.IsAllowToOrder && x.Product.IsPublished && !x.Product.IsDeleted,
                    ProductImage = _mediaService.GetThumbnailUrl(x.Product.ThumbnailImage),
                    Quantity = x.Quantity,
                }).ToList();

            cartVm.SubTotal = cartVm.Items.Sum(x => x.Quantity * x.ProductPrice);

            return cartVm;
        }

        public async Task MigrateCart(string fromUserId, string toUserId)
        {
            var cartFrom = await GetActiveCart(fromUserId, fromUserId);
            if (cartFrom != null && cartFrom.Items.Any())
            {
                var cartTo = await GetActiveCart(toUserId, toUserId);
                if (cartTo == null)
                {
                    cartTo = new Cart
                    {
                        CustomerId = toUserId,
                        CreatedBy = toUserId,
                        Id = Guid.NewGuid().ToString(),
                        CreatedOn = DateTime.UtcNow,
                        ModifiedOn = DateTime.UtcNow,
                        ModifiedBy = toUserId,
                        IsActive = true
                    };

                    await _dbContext.Set<Cart>().AddAsync(cartTo);
                }

                foreach (var fromItem in cartFrom.Items)
                {
                    var toItem = cartTo.Items.FirstOrDefault(x => x.ProductId == fromItem.ProductId);
                    if (toItem == null)
                    {
                        toItem = new CartItem
                        {
                            Cart = cartTo,
                            ProductId = fromItem.ProductId,
                            Quantity = fromItem.Quantity,
                            CreatedOn = DateTime.UtcNow,
                            CreatedBy = toUserId,
                            Id = Guid.NewGuid().ToString(),
                            ModifiedOn = DateTime.UtcNow,
                            ModifiedBy = toUserId,
                        };
                        cartTo.Items.Add(toItem);
                    }
                    else
                    {
                        toItem.Quantity = toItem.Quantity + fromItem.Quantity;
                    }
                }

                _dbContext.Set<Cart>().Remove(cartFrom);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
