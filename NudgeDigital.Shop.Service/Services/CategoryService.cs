﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class CategoryService : ICategoryService
    {
        IEntityService _entityService;
        DbContext _dbContext;

        private const string CategoryEntityTypeId = "Category";

        public CategoryService(IDatabaseContextFactory databaseContextFactory, IEntityService entityService)
        {
            _entityService = entityService;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public async Task<Category> Create(Category category)
        {
            category.Slug = await _entityService.ToSafeSlug(category.Slug, category.Id, CategoryEntityTypeId);

            var createdCategory = await _dbContext.Set<Category>().AddAsync(category);
            if (await _dbContext.SaveChangesAsync() > 0)
            {
                await _entityService.Add(category.Name, category.Slug, category.Id, CategoryEntityTypeId);
                return createdCategory.Entity;
            }

            return null;
        }

        public async Task<Category> Delete(Category category)
        {
            var cat = await _dbContext.Set<Category>().FindAsync(category.Id);
            await _entityService.Remove(category.Id, CategoryEntityTypeId);
            cat.IsDeleted = true;

            await _dbContext.SaveChangesAsync();
            return cat;

        }

        public IQueryable<Category> Get() => _dbContext.Set<Category>();

        public async Task<Category> Get(string id) => await _dbContext.Set<Category>().FindAsync(id);

        public async Task<Category> Update(Category category)
        {
            category.Slug = await _entityService.ToSafeSlug(category.Slug, category.Id, CategoryEntityTypeId);

            await _entityService.Update(category.Name, category.Slug, category.Id, CategoryEntityTypeId);

            var existingCategory = await _dbContext.Set<Category>().FindAsync(category.Id);
            existingCategory = category;

            if (await _dbContext.SaveChangesAsync() > 0)
                return existingCategory;

            return null;
        }
    }
}
