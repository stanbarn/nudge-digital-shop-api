﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using NudgeDigital.Shop.Core.Services;
using NudgeDigital.Shop.Core.Settings;
using System.Diagnostics.Contracts;
using System.IO;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class AzureBlobStorageService : IStorageService
    {
        private CloudBlobContainer _blobContainer;
        private string _publicEndpoint = AppSettings.AzureBlobEndPoint;

        public AzureBlobStorageService()
        {
            var storageConnectionString = AppSettings.AzureBlobConnectionString;
            var containerName = AppSettings.AzureBlobContainerName;

            Contract.Requires(string.IsNullOrWhiteSpace(storageConnectionString));
            Contract.Requires(string.IsNullOrWhiteSpace(containerName));

            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);

            _blobContainer = storageAccount.CreateCloudBlobClient().GetContainerReference(containerName);

            if (string.IsNullOrWhiteSpace(_publicEndpoint))
                _publicEndpoint = _blobContainer.Uri.AbsoluteUri;

        }
        public async Task DeleteMediaAsync(string fileName) => await _blobContainer.GetBlockBlobReference(fileName).DeleteIfExistsAsync();

        public string GetMediaUrl(string fileName) => $"{_publicEndpoint}/{fileName}";

        public async Task SaveMediaAsync(Stream mediaBinaryStream, string fileName, string mimeType = null)
        {
            await _blobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Container });
            await _blobContainer.CreateIfNotExistsAsync();

            await _blobContainer.GetBlockBlobReference(fileName).UploadFromStreamAsync(mediaBinaryStream);
        }
    }
}
