﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class EntityService : IEntityService
    {
        private readonly DbContext _dbContext;

        public EntityService(IDatabaseContextFactory databaseContextFactory)
        {
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public async Task<string> ToSafeSlug(string slug, string entityId, string entityTypeId)
        {
            var i = 2;
            while (true)
            {
                var entity = await _dbContext.Set<Entity>().FirstOrDefaultAsync(x => x.Slug == slug);
                if (entity != null && !(entity.EntityId == entityId && entity.EntityTypeId == entityTypeId))
                {
                    slug = string.Format("{0}-{1}", slug, i);
                    i++;
                }
                else
                    break;
            }

            return slug;
        }

        public async Task<Entity> Get(string entityId, string entityTypeId)
            => await _dbContext.Set<Entity>().FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityTypeId == entityTypeId);

        public async Task Add(string name, string slug, string entityId, string entityTypeId)
        {
            var entity = new Entity
            {
                Name = name,
                Slug = slug,
                EntityId = entityId,
                EntityTypeId = entityTypeId,
            };

            await _dbContext.Set<Entity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(string newName, string newSlug, string entityId, string entityTypeId)
        {
            var entity = await _dbContext.Set<Entity>().FirstAsync(x => x.EntityId == entityId && x.EntityTypeId == entityTypeId);
            entity.Name = newName;
            entity.Slug = newSlug;
        }

        public async Task Remove(string entityId, string entityTypeId)
        {
            var entity = await _dbContext.Set<Entity>().FirstOrDefaultAsync(x => x.EntityId == entityId && x.EntityTypeId == entityTypeId);

            if (entity != null)
            {
                //await _mediator.Publish(new EntityDeleting { EntityId = entity.Id });
                _dbContext.Set<Entity>().Remove(entity);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
