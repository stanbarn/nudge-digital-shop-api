﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class ProductService : IProductService
    {
        private const string ProductEntityTypeId = "Product";
        private DbContext _dbContext;
        private IEntityService _entityService;

        public ProductService(IDatabaseContextFactory databaseContextFactory, IEntityService entityService)
        {
            _entityService = entityService;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public IQueryable<Product> Get() => _dbContext.Set<Product>();

        public async Task<Product> Get(string id) => await _dbContext.Set<Product>().FindAsync(id);

        public async Task<Product> Create(Product product)
        {
            product.Slug = await _entityService.ToSafeSlug(product.Slug, product.Id, ProductEntityTypeId);

            var createdProduct = await _dbContext.Set<Product>().AddAsync(product);
            await _dbContext.SaveChangesAsync();

            await _entityService.Add(product.Name, product.Slug, product.Id, ProductEntityTypeId);

            return createdProduct.Entity;
        }

        public async Task Update(Product product)
        {
            var slug = _entityService.Get(product.Id, ProductEntityTypeId);
            if (product.IsVisibleIndividually)
            {
                product.Slug = await _entityService.ToSafeSlug(product.Slug, product.Id, ProductEntityTypeId);
                if (slug != null)
                    await _entityService.Update(product.Name, product.Slug, product.Id, ProductEntityTypeId);

                else
                    await _entityService.Add(product.Name, product.Slug, product.Id, ProductEntityTypeId);
            }
            else
            {
                if (slug != null)
                    await _entityService.Remove(product.Id, ProductEntityTypeId);
            }

            try
            {
                _dbContext.Update(product);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task Delete(Product product)
        {
            product.IsDeleted = true;
            await _entityService.Remove(product.Id, ProductEntityTypeId);
            await _dbContext.SaveChangesAsync();
        }
    }
}
