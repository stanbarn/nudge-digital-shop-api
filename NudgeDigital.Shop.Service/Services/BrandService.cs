﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class BrandService : IBrandService
    {
        private readonly IEntityService _entityService;
        private readonly DbContext _dbContext;

        private const string BrandEntityTypeId = "Brand";

        public BrandService(IDatabaseContextFactory databaseContextFactory, IEntityService entityService)
        {
            _entityService = entityService;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public IQueryable<Brand> Get() => _dbContext.Set<Brand>();

        public async Task<Brand> Get(string id) => await _dbContext.Set<Brand>().FindAsync(id);

        public async Task<Brand> Create(Brand brand)
        {
            brand.Slug = await _entityService.ToSafeSlug(brand.Slug, brand.Id, BrandEntityTypeId);
            var createdbrand = await _dbContext.Set<Brand>().AddAsync(brand);

            if (await _dbContext.SaveChangesAsync() > 0)
            {
                await _entityService.Add(brand.Name, brand.Slug, brand.Id, BrandEntityTypeId);
                return createdbrand.Entity;
            }

            return null;
        }

        public async Task<Brand> Delete(string id)
        {
            var brand = await _dbContext.Set<Brand>().FindAsync(id);
            brand.IsDeleted = true;
            await _entityService.Remove(brand.Id, BrandEntityTypeId);

            if (await _dbContext.SaveChangesAsync() > 0)
                return brand;

            return null;
        }

        public async Task Delete(Brand brand)
        {
            brand.IsDeleted = true;
            await _entityService.Remove(brand.Id, BrandEntityTypeId);
            _dbContext.SaveChanges();
        }

        public async Task<Brand> Update(Brand brand)
        {
            brand.Slug = await _entityService.ToSafeSlug(brand.Slug, brand.Id, BrandEntityTypeId);
            await _entityService.Update(brand.Name, brand.Slug, brand.Id, BrandEntityTypeId);

            var existingBrand = await _dbContext.Set<Brand>().FindAsync(brand.Id);
            existingBrand = brand;

            if (await _dbContext.SaveChangesAsync() > 0)
                return existingBrand;

            return null;
        }
    }
}
