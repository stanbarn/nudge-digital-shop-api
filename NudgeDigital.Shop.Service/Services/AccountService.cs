﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Enums;
using NudgeDigital.Shop.Core.Exceptions;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Models.Messages;
using NudgeDigital.Shop.Core.Services;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class AccountService : IAccountService
    {
        private readonly DbContext _dbContext;
        public AccountService(IDatabaseContextFactory databaseContextFactory)
        {
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public async Task<User> CreateAsync(User account)
        {
            var createdAccount = await _dbContext.Set<User>().AddAsync(account);
            if (await _dbContext.SaveChangesAsync() > 0)
            {
                return createdAccount.Entity;
            }

            return null;
        }

        public IQueryable<User> Get() => _dbContext.Set<User>();

        public async Task<User> GetAsync(string AccountId) => await _dbContext.Set<User>()
            .SingleOrDefaultAsync(x => x.Id == AccountId);

        public async Task<User> UpdateAsync(User Account)
        {
            var updatedAccount = _dbContext.Set<User>()
                .Find(Account.Id);
            if (updatedAccount != null)
            {
                updatedAccount = Account;

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return updatedAccount;
                }
                return null;
            }


            throw new ServiceException(new SimpleMessage { Code = -1, Message = string.Format("Account {0} does not exist", Account.Id), Level = MessageLevel.ERROR });

        }

        public async Task<User> AuthenticateAsync(string username, string passwordHash) => await _dbContext.Set<User>()
                .SingleOrDefaultAsync(x => x.EmailAddress == username && x.PasswordHash == passwordHash);

        public async Task<User> ActivateAccountAsync(string email, string auth)
        {
            var account = await _dbContext.Set<User>()
                .SingleOrDefaultAsync();


            _dbContext.Set<User>().Update(account);

            if (await _dbContext.SaveChangesAsync() > 0)
            {
                return account;
            }

            return null;
        }

        public async Task<User> GetByEmailAsync(string email) => await _dbContext.Set<User>()
                .SingleOrDefaultAsync();

        public async Task<bool> IsEmailExistsAsync(string emailAddress)
        {
            var account = await _dbContext.Set<User>()
                .SingleOrDefaultAsync();
            if (account != null)
                return true;

            return false;
        }

        public async Task<User> DeleteAsync(string AccountId)
        {
            var account = await _dbContext.Set<User>().FindAsync(AccountId);
            if (account != null)
            {
                _dbContext.Set<User>().Remove(account);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return account;
                }
            }
            return null;
        }

    }
}
