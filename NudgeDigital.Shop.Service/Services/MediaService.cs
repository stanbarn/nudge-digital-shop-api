﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System.IO;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Service.Services
{
    public class MediaService : IMediaService
    {
        private readonly DbContext _dbContext;
        private readonly IStorageService _storageService;

        public MediaService(IDatabaseContextFactory databaseContextFactory, IStorageService storageService)
        {
            _storageService = storageService;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        public string GetMediaUrl(Media media)
        {
            if (media == null)
                return GetMediaUrl("no-image.png");

            return GetMediaUrl(media.FileName);
        }

        public string GetMediaUrl(string fileName) => _storageService.GetMediaUrl(fileName);

        public string GetThumbnailUrl(Media media) => GetMediaUrl(media);

        public async Task SaveMediaAsync(Stream mediaBinaryStream, string fileName, string mimeType = null)
            => await _storageService.SaveMediaAsync(mediaBinaryStream, fileName, mimeType);

        public async Task DeleteMediaAsync(Media media)
        {
            _dbContext.Set<Media>().Remove(media);
            await DeleteMediaAsync(media.FileName);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteMediaAsync(string fileName)
            => await _storageService.DeleteMediaAsync(fileName);
    }
}
