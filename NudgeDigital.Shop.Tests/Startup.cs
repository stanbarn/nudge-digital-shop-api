﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Services;
using NudgeDigital.Shop.Data;
using NudgeDigital.Shop.Service.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit.DependencyInjection;
using Xunit.DependencyInjection.Logging;

namespace NudgeDigital.Shop.Tests
{
    public class Startup
    {
        public void ConfigureHost(IHostBuilder hostBuilder)
        {
            hostBuilder
                .ConfigureAppConfiguration(builder =>
                {
                    builder
                        .AddInMemoryCollection(new Dictionary<string, string>()
                        {
                            {"UserName", "Alice"}
                        })
                        .AddJsonFile("appsettings.json")
                        ;
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddTransient<IAccountService, AccountService>();
                    services.AddTransient<IBrandService, BrandService>();
                    services.AddTransient<ICartService, CartService>();
                    services.AddTransient<ICategoryService, CategoryService>();
                    services.AddTransient<IEntityService, EntityService>();
                    services.AddTransient<IMediaService, MediaService>();
                    services.AddTransient<IProductService, ProductService>();
                    services.AddTransient<IStorageService, AzureBlobStorageService>();
                    services.AddTransient<IWorkContext, WorkContext>();
                    //Inject DbCOntext Factory
                    services.AddTransient<IDatabaseContextFactory, DatabaseContextFactory>();
                    services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Debug));
                });
        }

        public void ConfigureServices(IServiceCollection services, HostBuilderContext hostBuilderContext)
        {

        }

        public void Configure(ILoggerFactory loggerFactory, ITestOutputHelperAccessor accessor) =>
            loggerFactory.AddProvider(new XunitTestOutputLoggerProvider(accessor, delegate { return true; }));
    }
}
