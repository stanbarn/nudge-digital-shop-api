﻿using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NudgeDigital.Shop.Tests.Services
{
    public class AccountServiceTest
    {
        private IAccountService _accountService;
        User user = new User
        {
            Id = Guid.NewGuid().ToString(),
            CreatedOn = DateTime.UtcNow,
            EmailAddress = "systemtest@nudgedigital.com",
            EmailConfirmed = false,
            FullName = "System Test User",
            IsDeleted = false,
            PasswordHash = AccountHelper.GeneratePasswordHash("STanTestPass??.@"),
            PhoneNumberConfirmed = false,
            ModifiedOn = DateTime.UtcNow,
            UserName = "systemtest@nudgedigital.com",
            CreatedBy = "system test",
            ModifiedBy = "system test",
            PhoneNumber = "256702173541"
        };

        public AccountServiceTest(IAccountService accountService) => _accountService = accountService;

        [Fact]
        public async Task Test_That_Can_Add_Account()
        {
            var account = await _accountService.CreateAsync(user);
            Assert.NotNull(account);
        }

        [Fact]
        public async Task Test_That_Can_Get_Account()
        {
            var account = await _accountService.GetAsync(user.Id);
            Assert.NotNull(account);
            Assert.Equal(account.Id, user.Id);
        }

        [Fact]
        public async Task Test_That_Can_Authenticate_Account_Successful()
        {
            var account = await _accountService.AuthenticateAsync(user.EmailAddress, "STanTestPass??.@");
            Assert.NotNull(account);
            Assert.Equal(account.Id, user.Id);
        }

        [Fact]
        public async Task Test_That_Can_Authenticate_Account_Fail()
        {
            var account = await _accountService.AuthenticateAsync(user.EmailAddress, "SanTestPass?.@");
            Assert.Null(account);
        }

        [Fact]
        public async Task Test_That_Can_Update_Account()
        {
            var update_user = user;
            update_user.ModifiedBy = "system update";
            update_user.ModifiedOn = DateTime.UtcNow;

            var updatedUser = await _accountService.UpdateAsync(update_user);
            Assert.Equal(updatedUser.ModifiedBy, update_user.ModifiedBy);
            Assert.NotNull(updatedUser);
        }
    }
}
