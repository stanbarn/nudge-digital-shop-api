﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NudgeDigital.Shop.Tests.Services
{
    public class BrandServiceTest
    {
        private IBrandService _brandService;
        Brand newBrand = new Brand
        {
            CreatedBy = Guid.NewGuid().ToString(),
            CreatedOn = DateTime.UtcNow,
            Description = "Samsung Phones",
            Id = Guid.NewGuid().ToString(),
            IsPublished = true,
            ModifiedBy = Guid.NewGuid().ToString(),
            ModifiedOn = DateTime.UtcNow,
            Name = "Samsung",
            Slug = "samsung"
        };

        public BrandServiceTest(IBrandService brandService) => _brandService = brandService;

        [Fact]
        public async Task Test_That_Can_Add_Brand()
        {
            var brand = await _brandService.Create(newBrand);
            Assert.NotNull(brand);
            Assert.Equal(brand.Id, newBrand.Id);

        }

        [Fact]
        public async Task Test_That_Can_Get_Brand()
        {
            var brand = await _brandService.Get(newBrand.Id);
            Assert.NotNull(brand);
            Assert.Equal(brand.Id, newBrand.Id);
        }

        [Fact]
        public async Task Test_That_Can_Get_AllBrand()
        {
            var brands = await _brandService.Get().ToListAsync();
            Assert.NotNull(brands);
        }

        [Fact]
        public async Task Test_That_Can_Update_Brand()
        {
            var update_brand = newBrand;
            update_brand.ModifiedBy = "system update";
            update_brand.ModifiedOn = DateTime.UtcNow;

            var updatedBrand = await _brandService.Update(update_brand);

            Assert.NotNull(updatedBrand);
        }

    }
}
