﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NudgeDigital.Shop.Tests.Services
{
    public class CategoryServiceTest
    {
        private ICategoryService _categoryService;
        Category newCategory = new Category
        {
            Id = Guid.NewGuid().ToString(),
            CreatedBy = "system tests",
            ModifiedBy = "system tests",
            CreatedOn = DateTime.UtcNow,
            Description = "system tests",
            DisplayOrder = 1,
            IncludeInMenu = true,
            IsPublished = true,
            ModifiedOn = DateTime.UtcNow,
            Slug = "smart-phones",
            Name = "Smart Phones"
        };

        public CategoryServiceTest(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Fact]
        public async Task Test_That_Can_Add_category()
        {
            var category = await _categoryService.Create(newCategory);
            Assert.NotNull(category);
        }

        [Fact]
        public async Task Test_That_Can_Get_category()
        {
            var category = await _categoryService.Get(newCategory.Id);
            Assert.NotNull(category);
        }

        [Fact]
        public async Task Test_That_Can_Get_Allcategory()
        {
            var categories = await _categoryService.Get().CountAsync();
            Assert.True(categories > 0);
        }

        [Fact]
        public async Task Test_That_Can_Update_category()
        {
            newCategory.ModifiedBy = "system test modify";
            newCategory.ModifiedOn = DateTime.UtcNow;

            var update = await _categoryService.Update(newCategory);
            Assert.NotNull(update);
        }
    }
}
