﻿using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NudgeDigital.Shop.Tests.Services
{
    public class EntityServiceTest
    {
        private IEntityService _entityService;

        Entity newEntity = new Entity
        {
            Name = "test entity",
            Slug = "test-entity",
            EntityId = Guid.NewGuid().ToString(),
            EntityTypeId = "Brand"
        };

        public EntityServiceTest(IEntityService entityService)
        {
            _entityService = entityService;
        }


        [Fact]
        public async Task Test_That_Can_Get_Entity()
        {
            var entity = await _entityService.Get(newEntity.EntityId, newEntity.EntityTypeId);
            Assert.NotNull(entity);

        }

        [Fact]
        public async Task Test_That_Can_Add_Entity()
        {
            await _entityService.Add(newEntity.Name, newEntity.Slug, newEntity.EntityId, newEntity.EntityTypeId);
        }

        [Fact]
        public async Task Test_That_Can_Update_Entity()
        {
            await _entityService.Update("Test Update", newEntity.Slug, newEntity.EntityId, newEntity.EntityTypeId);
        }
    }
}
