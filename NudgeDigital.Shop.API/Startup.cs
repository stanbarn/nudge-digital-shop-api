using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.API.Middleware;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using NudgeDigital.Shop.Core.Settings;
using NudgeDigital.Shop.Data;
using NudgeDigital.Shop.Service.Services;

namespace NudgeDigital.Shop.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddHttpClient();

            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IEntityService, EntityService>();
            services.AddTransient<IMediaService, MediaService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IStorageService, AzureBlobStorageService>();
            services.AddTransient<IWorkContext, WorkContext>();
            services.AddHttpContextAccessor();

            //Inject DbCOntext Factory
            services.AddTransient<IDatabaseContextFactory, DatabaseContextFactory>();

            //Inject Database Context for Migration purposes
            services.AddDbContext<NudgeDigitalDatabaseContext>(options =>
            options.UseSqlServer(AppSettings.DatabaseConnectionString, b => b.MigrationsAssembly("NudgeDigital.Shop.API")));

            //Add JWT Authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ValidIssuer = AppSettings.AuthIssuer,
                    ValidAudience = "All",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.AuthSecretKey))
                });

            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddApiVersioning(o => o.DefaultApiVersion = new ApiVersion(1, 0));

            // Register the Swagger services
            services.AddSwaggerDocument(config => {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Nudge Digital Shop";
                    document.Info.Description = "This is Nudge digital's Shop API";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Stanley Barnabas Nagwere",
                        Email = "stanleybarna@xente.co",
                        Url = "landlordhub.site"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = "https://example.com/license"
                    };

                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            // Add custom error handling middleware
            app.UseCustomExceptionHandler();

            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();

            //Register JWT Authentication
            app.UseAuthentication();

            app.UseRouting();
            app.UseAuthorization();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
