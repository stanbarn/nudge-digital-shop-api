﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NLog;
using NudgeDigital.Shop.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private const string contentType = "application/json";
        private readonly RequestDelegate _request;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlerMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _request = next;
        }

        /// <summary>
        /// Invokes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public Task Invoke(HttpContext context) => this.InvokeAsync(context);

        async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _request(context);
            }
            catch (Exception exception)
            {
                //set http status codes and content type
                context.Response.StatusCode = ConfigurateExceptionTypes(exception);
                context.Response.ContentType = contentType;

                Logger logger = LogManager.GetCurrentClassLogger();
                logger.Error(new Exception(string.Format("--Message {0}", exception.Message), exception.InnerException));

                //return error response to the client
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                {
                    Status = "fail",
                    exception.Message
                }));

                //clear the headers
                context.Response.Headers.Clear();
            }
        }

        /// <summary>
        /// Configurates/maps exception to the proper HTTP error Type
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        private static int ConfigurateExceptionTypes(Exception exception)
        {
            int httpStatusCode;
            // Exception type To Http Status configuration 
            switch (exception)
            {
                case var _ when exception is ValidationException:
                    httpStatusCode = (int)HttpStatusCode.BadRequest;
                    break;

                case var _ when exception is ServiceException:
                case var _ when exception is ProductException:
                    httpStatusCode = (int)HttpStatusCode.OK;
                    break;

                default:
                    httpStatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            return httpStatusCode;
        }
    }
}
