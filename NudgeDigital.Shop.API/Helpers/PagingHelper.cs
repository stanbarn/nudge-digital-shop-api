﻿using NudgeDigital.Shop.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Helpers
{
    public class PagingHelper<T>
    {
        public static PagingModel<T> GeneratePaging(IEnumerable<T> collection, PagingParameterModel pagingParameterModel)
        {
            int count = collection.Count();
            int CurrentPage = pagingParameterModel.PageNumber;
            int PageSize = pagingParameterModel.PageSize;
            int TotalCount = count;
            int TotalPages = (int)Math.Ceiling(count / (double)PageSize);

            var collectionResult = collection
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize).ToList();

            var PreviousPage = CurrentPage > 1 ? true : false;
            var NextPage = CurrentPage < TotalPages ? true : false;

            return new PagingModel<T>
            {
                CurrentPage = CurrentPage,
                NextPage = NextPage,
                PageSize = PageSize,
                PreviousPage = PreviousPage,
                Collection = collectionResult,
                TotalCount = TotalCount,
                TotalPages = TotalPages
            };
        }

        public static PagingModel<TResult> GeneratePaging<TModel, TResult>(IQueryable<TModel> collection, PagingParameterModel pagingParameterModel, Expression<Func<TModel, TResult>> selector)
        {
            int count = collection.Count();
            int CurrentPage = pagingParameterModel.PageNumber;
            int PageSize = pagingParameterModel.PageSize;
            int TotalCount = count;
            int TotalPages = (int)Math.Ceiling(count / (double)PageSize);

            var collectionResult = collection
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize);

            var items = collectionResult.Select(selector).ToList();

            var PreviousPage = CurrentPage > 1 ? true : false;
            var NextPage = CurrentPage < TotalPages ? true : false;

            return new PagingModel<TResult>
            {
                CurrentPage = CurrentPage,
                NextPage = NextPage,
                PageSize = PageSize,
                PreviousPage = PreviousPage,
                Collection = items,
                TotalCount = TotalCount,
                TotalPages = TotalPages
            };
        }
    }
}
