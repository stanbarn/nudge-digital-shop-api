﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Enums;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Helpers
{
    public class WorkContext : IWorkContext
    {
        private const string UserGuidCookiesName = "NudgeUserGuid";
        private const string GuestRoleId = "GUEST";

        private User _currentUser;
        private HttpContext _httpContext;
        private IAccountService _accountService;

        public WorkContext(IHttpContextAccessor contextAccessor, IAccountService accountService)
        {
            _httpContext = contextAccessor.HttpContext;
            _accountService = accountService;
        }

        public async Task<User> GetCurrentUser()
        {
            if (_currentUser != null)
                return _currentUser;

            var userId = _httpContext.User.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;

            _currentUser = await _accountService.GetAsync(userId);

            return _currentUser;

        }
    }
}
