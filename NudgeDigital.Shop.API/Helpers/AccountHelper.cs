﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Helpers
{
    public class AccountHelper
    {
        
        public static DateTime NewApiPasswordExpiry()
        {
            return DateTime.UtcNow.AddYears(2);
        }

        public static void ValidatePassword(string input, int minlength = 10)
        {
            if (minlength < 10)
            {
                throw new ValidationException("Password Minimum Length Cannot Less Than 10");
            }

            // not empty
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ValidationException("Password Is Empty");
            }

            // not white space
            if (input.Any(x => char.IsWhiteSpace(x)))
            {
                throw new ValidationException("Password Cannot Contain Whitespace");
            }

            // not too short
            if (input.Length < minlength)
            {
                throw new ValidationException($"Password Length Cannot Cannot Be Less Than {minlength}");
            }

            // has numbers & chars
            if (!input.Any(x => char.IsLetterOrDigit(x)))
            {
                throw new ValidationException("Password Must Contain Alpha-Numeric Character");
            }

            // has upper  case
            if (!input.Any(x => char.IsUpper(x)))
            {
                throw new ValidationException("Password Must Contain Alphabetic Upper Case Characters");
            }

            // has lower case
            if (!input.Any(x => char.IsLower(x)))
            {
                throw new ValidationException("Password Must Contain Alphabetic Lower Case Characters");
            }

            var chars = "[]{}-,_+=%^$*;()?#@|!".ToCharArray();

            // has special chars
            if (input.Any(x => !char.IsLetterOrDigit(x) && !chars.Contains(x)))
            {
                throw new ValidationException("Password Must Contain Non-Alphabetic Characters");
            }

            var validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]{}-,_+=%^$*;()?#@|!";

            // has valid chars
            if (!input.All(x => validChars.Contains(x)))
            {
                throw new ValidationException($"Password Cannot Contain {string.Join(",", input.Where(x => !validChars.Contains(x)))}");
            }
        }

        public static string GeneratePasswordHash(string password)
        {
            using (var hashAlgorith = SHA512.Create())
            {
                return Convert.ToBase64String(hashAlgorith.ComputeHash(Encoding.UTF8.GetBytes(password)));
            }
        }

    }
}
