﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductOptionsController : ControllerBase
    {
        private readonly DbContext _dbContext;
        private readonly IWorkContext _workContext;

        public ProductOptionsController(IDatabaseContextFactory databaseContextFactory, IWorkContext workContext)
        {
            _workContext = workContext;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var options = _dbContext.Set<ProductOption>().ToList();
            return Ok(new
            {
                Status = "success",
                Data = options
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var productOption = await _dbContext.Set<ProductOption>().FirstOrDefaultAsync(x => x.Id == id);
            var model = new ProductOptionRequestModel
            {
                Id = productOption.Id,
                Name = productOption.Name
            };

            return Ok(new {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] ProductOptionRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _dbContext.Set<ProductOption>().SingleOrDefaultAsync(x => x.Name == model.Name) != null)
                {
                    ModelState.AddModelError("", $"Product Option With Name {model.Name} Already Exists");
                    return BadRequest(ModelState);
                }

                var currentUser = await _workContext.GetCurrentUser();
                var productOption = new ProductOption
                {
                    Id = Guid.NewGuid().ToString(),
                    CreatedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedBy = currentUser.Id,
                    ModifiedOn = DateTime.UtcNow,
                    Name = model.Name
                };

                var addedOption = await _dbContext.Set<ProductOption>().AddAsync(productOption);
                await _dbContext.SaveChangesAsync();

                return Ok(new
                {
                    Status = "success",
                    Message = "Product option added successfully.",
                    Data = addedOption?.Entity
                });
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(string id, [FromBody] ProductOptionRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var productOption = await _dbContext.Set<ProductOption>().FirstOrDefaultAsync(x => x.Id == id);
                productOption.Name = model.Name;

                await _dbContext.SaveChangesAsync();
                return Ok(new
                {
                    Status = "success",
                    Message = "Ok"
                });
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(string id)
        {
            var productOption = await _dbContext.Set<ProductOption>().FirstOrDefaultAsync(x => x.Id == id);

            if (productOption == null)
                return NotFound();

            _dbContext.Remove(productOption);
            await _dbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
