﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly DbContext _dbContext;
        private readonly ICartService _cartService;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;

        public CartController(
            IDatabaseContextFactory databaseContextFactory,
            ICartService cartService,
            IMediaService mediaService,
            IWorkContext workContext)
        {
            _dbContext = databaseContextFactory.GetDatabaseContext();
            _cartService = cartService;
            _mediaService = mediaService;
            _workContext = workContext;
        }

        [Authorize]
        [HttpGet("customers/{customerId}/cart")]
        public async Task<IActionResult> List(string customerId)
        {
            var currentUser = await _workContext.GetCurrentUser();
            var cart = await _cartService.GetActiveCartDetails(customerId, currentUser.Id);

            return Ok(cart);
        }

        [Authorize]
        [HttpPost("customers/{customerId}/add-cart-item")]
        public async Task<IActionResult> AddToCart(string customerId, [FromBody] AddToCartModel model)
        {
            var currentUser = await _workContext.GetCurrentUser();
            if (User.IsInRole("customer") && customerId != currentUser.Id)
            {
                return StatusCode(403);
            }
            await _cartService.AddToCart(customerId, currentUser.Id, model.ProductId, model.Quantity);

            return Accepted();
        }

        [Authorize]
        [HttpPut("items/{itemId}")]
        public async Task<IActionResult> UpdateQuantity(string itemId, [FromBody] CartQuantityUpdate model)
        {
            var currentUser = await _workContext.GetCurrentUser();
            var cartItem = new CartItem();

            if (User.IsInRole("admin"))
                cartItem = await _dbContext.Set<CartItem>().FirstOrDefaultAsync(x => x.Id == itemId && x.Cart.CreatedBy == currentUser.Id);
            else
                cartItem = await _dbContext.Set<CartItem>().FirstOrDefaultAsync(x => x.Id == itemId);

            if (cartItem == null)
                return NotFound();

            cartItem.Quantity = model.Quantity;
            await _dbContext.SaveChangesAsync();

            return Accepted();
        }

        [Authorize]
        [HttpDelete("items/{itemId}")]
        public async Task<IActionResult> Remove(string itemId)
        {
            var currentUser = await _workContext.GetCurrentUser();
            var cartItem = await _dbContext.Set<CartItem>().FirstOrDefaultAsync(x => x.Id == itemId && x.Cart.CreatedBy == currentUser.Id);
            if (cartItem == null)
            {
                return NotFound();
            }

            _dbContext.Remove(cartItem);
            await _dbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
