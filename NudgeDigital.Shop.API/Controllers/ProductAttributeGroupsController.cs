﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductAttributeGroupsController : ControllerBase
    {
        private DbContext _dbContext;
        private readonly IWorkContext _workContext;

        public ProductAttributeGroupsController(IDatabaseContextFactory databaseContextFactory, IWorkContext workContext)
        {
            _workContext = workContext;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var attributeGroups = await _dbContext
                .Set<ProductAttributeGroup>()
                .Select(x => new ProductAttributeGroupRequestVm
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToListAsync();

            return Ok(new
            {
                Status = "success",
                Data = attributeGroups
            });
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var productAttributeGroup = _dbContext
                .Set<ProductAttributeGroup>().FirstOrDefault(x => x.Id == id);
            var model = new ProductAttributeGroupRequestVm
            {
                Id = productAttributeGroup.Id,
                Name = productAttributeGroup.Name
            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] ProductAttributeGroupRequestVm model)
        {
            if (ModelState.IsValid)
            {
                if (await _dbContext.Set<ProductAttributeGroup>().SingleOrDefaultAsync(x => x.Name == model.Name) != null)
                {
                    ModelState.AddModelError("", $"Product Attribute Group With Name {model.Name} Already Exists");
                    return BadRequest(ModelState);
                }

                var currentUser = await _workContext.GetCurrentUser();

                var productAttributeGroup = new ProductAttributeGroup
                {
                    Id = Guid.NewGuid().ToString(),
                    CreatedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedBy = currentUser.Id,
                    ModifiedOn = DateTime.UtcNow,
                    Name = model.Name
                };

                _dbContext.Add(productAttributeGroup);
                _dbContext.SaveChanges();

                return Ok(new
                {
                    Status = "success",
                    Message = "Product Attribute group added successfully.",
                    Data = productAttributeGroup
                });
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize]
        public IActionResult Put(string id, [FromBody] ProductAttributeGroupRequestVm model)
        {
            if (ModelState.IsValid)
            {
                var productAttributeGroup = _dbContext
                .Set<ProductAttributeGroup>().FirstOrDefault(x => x.Id == id);
                productAttributeGroup.Name = model.Name;

                _dbContext.SaveChanges();

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(string id)
        {
            var productAttributeGroup = _dbContext
                .Set<ProductAttributeGroup>().FirstOrDefault(x => x.Id == id);
            if (productAttributeGroup == null)
            {
                return NotFound();
            }

            _dbContext.Remove(productAttributeGroup);
            return NoContent();
        }
    }
}
