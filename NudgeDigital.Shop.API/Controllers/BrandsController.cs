﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class BrandsController : ControllerBase
    {
        private readonly DbContext _dbContext;
        private readonly IBrandService _brandService;
        private readonly IWorkContext _workContext;
        private readonly IMediaService _mediaService;

        public BrandsController(IDatabaseContextFactory brandRepository, IBrandService brandService, IWorkContext workContext, IMediaService mediaService)
        {
            _dbContext = brandRepository.GetDatabaseContext();
            _brandService = brandService;
            _workContext = workContext;
            _mediaService = mediaService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var brandList = await _dbContext.Set<Brand>().Where(x => !x.IsDeleted).Select(y => new
            {
                y.CreatedOn,
                y.Description,
                y.Id,
                y.IsPublished,
                y.ModifiedOn,
                y.Name,
                y.Slug,
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(y.ThumbnailImage)

            }).ToListAsync();

            return Ok(new
            {
                Status = "success",
                Data = brandList
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var brand = await _dbContext.Set<Brand>().FirstOrDefaultAsync(x => x.Id == id);
            var model = new
            {
                brand.CreatedOn,
                brand.Description,
                brand.Id,
                brand.IsPublished,
                brand.ModifiedOn,
                brand.Name,
                brand.Slug,
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(brand.ThumbnailImage)

            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromForm] BrandRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _brandService.Get().SingleOrDefaultAsync(x => x.Name == model.Name) != null)
                {
                    ModelState.AddModelError("", $"Brand With Name {model.Name} Already Exists");
                    return BadRequest(ModelState);
                }

                var currentUser = await _workContext.GetCurrentUser();

                var brand = new Brand
                {
                    Name = model.Name,
                    Slug = model.Slug,
                    IsPublished = model.IsPublished,
                    Id = Guid.NewGuid().ToString(),
                    CreatedBy = currentUser.Id,
                    ModifiedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow,
                };

                if (model.CategoryIds != null)
                {
                    if (model.CategoryIds.Any())
                    {
                        var brandCategories = new List<BrandCategory>();
                        foreach (var category in model.CategoryIds)
                        {
                            brandCategories.Add(new BrandCategory
                            {
                                BrandId = brand.Id,
                                CategoryId = category
                            });
                        }

                        brand.BrandCategories = brandCategories;
                    }
                }


                await SaveBrandMedias(model, brand);

                await _brandService.Create(brand);
                return Ok(new
                {
                    Status = "success",
                    message = "Brand added successfully.",
                    Data = brand
                });
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(string id, [FromForm] BrandRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var brand = _dbContext.Set<Brand>().FirstOrDefault(x => x.Id == id);
                if (brand == null)
                {
                    return NotFound();
                }

                brand.Name = model.Name;
                brand.Slug = model.Slug;
                brand.IsPublished = model.IsPublished;

                await _brandService.Update(brand);
                return Accepted();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(string id)
        {
            var brand = _dbContext.Set<Brand>().FirstOrDefault(x => x.Id == id);
            if (brand == null)
            {
                return NotFound();
            }

            await _brandService.Delete(brand.Id);
            return NoContent();
        }


        private async Task SaveBrandMedias(BrandRequestModel model, Brand brand)
        {
            if (model.ThumbnailImage != null)
            {
                var fileName = await SaveFile(model.ThumbnailImage);
                if (brand.ThumbnailImage != null)
                    brand.ThumbnailImage.FileName = fileName;

                else
                    brand.ThumbnailImage = new Media
                    {
                        FileName = fileName,
                        Id = Guid.NewGuid().ToString(),
                        CreatedBy = brand.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedBy = brand.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    };
            }
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }
    }
}
