﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;
using NudgeDigital.Shop.Core.Settings;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IAccountService _accountService;
        public TokenController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> GenerateAuthToken([FromBody] TokenModel login, bool includeRefreshToken)
        {
            var user = await _accountService.AuthenticateAsync(login.Username, AccountHelper.GeneratePasswordHash(login.Password));
            if (user == null)
            {
                return BadRequest(new { Error = "Invalid username or password" });
            }

            var claims = await BuildClaims(user);
            var token = GenerateToken(claims);
            if (includeRefreshToken)
            {
                var refreshToken = GenerateRefreshToken();
                user.RefreshTokenHash = AccountHelper.GeneratePasswordHash(refreshToken);

                await _accountService.UpdateAsync(user);
                return Ok(new 
                { 
                    Token = token,
                    RefreshToken = refreshToken,
                    UserId = user.Id 
                });
            }

            return Ok(new 
            { 
                Token = token, 
                UserId = user.Id 
            });
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> RefeshAuthToken(TokenRefreshModel model)
        {
            var principal = GetPrincipalFromExpiredToken(model.Token);
            if (principal == null)
            {
                return BadRequest(new { Error = "Invalid token" });
            }


            var user = await _accountService.GetAsync(principal.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value);

            if (user == null)
            {
                return Forbid();
            }

            var refToken = AccountHelper.GeneratePasswordHash(model.RefreshToken);

            if (user.RefreshTokenHash == AccountHelper.GeneratePasswordHash(model.RefreshToken))
            {
                var claims = await BuildClaims(user);
                var newToken = GenerateToken(claims);
                return Ok(new
                {
                    Token = newToken,
                    UserId = user.Id
                });
            }

            return Forbid();
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var jwtToken = new JwtSecurityToken(
                issuer: AppSettings.AuthIssuer,
                audience: "All",
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(int.Parse(AppSettings.AuthDurationInMinutes)),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.AuthSecretKey)), SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var randomNumberGenerator = RandomNumberGenerator.Create())
            {
                randomNumberGenerator.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = true,
                ValidIssuer = AppSettings.AuthIssuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.AuthSecretKey)),
                ValidateLifetime = false //in this case, we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

            if (!(securityToken is JwtSecurityToken jwtSecurityToken) || !string.Equals(jwtSecurityToken.Header.Alg, SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                return null;

            return principal;
        }

        private async Task<IList<Claim>> BuildClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.EmailAddress)
            };

            return claims;
        }
    }
}
