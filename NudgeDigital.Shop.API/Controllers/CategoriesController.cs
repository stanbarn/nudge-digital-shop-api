﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly DbContext _dbContext;
        private readonly ICategoryService _categoryService;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;

        public CategoriesController(IDatabaseContextFactory databaseContextFactory, ICategoryService categoryService, IMediaService mediaService, IWorkContext workContext)
        {
            _dbContext = databaseContextFactory.GetDatabaseContext();
            _categoryService = categoryService;
            _mediaService = mediaService;
            _workContext = workContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var categories = await _categoryService.Get().ToListAsync();
            return Ok(new
            {
                Status = "success",
                Data = categories
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var category = await _dbContext.Set<Category>().Include(x => x.ThumbnailImage).FirstOrDefaultAsync(x => x.Id == id);
            var model = new CategoryRequestModel
            {
                Id = category.Id,
                Name = category.Name,
                Slug = category.Slug,
                MetaTitle = category.MetaTitle,
                MetaKeywords = category.MetaKeywords,
                MetaDescription = category.MetaDescription,
                DisplayOrder = category.DisplayOrder,
                Description = category.Description,
                ParentId = category.ParentId,
                IncludeInMenu = category.IncludeInMenu,
                IsPublished = category.IsPublished,
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromForm] CategoryRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _categoryService.Get().SingleOrDefaultAsync(x => x.Name == model.Name) != null)
                {
                    ModelState.AddModelError("", $"Category With Name {model.Name} Already Exists");
                    return BadRequest(ModelState);
                }

                var currentUser = await _workContext.GetCurrentUser();

                var category = new Category
                {
                    Name = model.Name,
                    Slug = model.Slug,
                    MetaTitle = model.MetaTitle,
                    MetaKeywords = model.MetaKeywords,
                    MetaDescription = model.MetaDescription,
                    DisplayOrder = model.DisplayOrder,
                    Description = model.Description,
                    ParentId = model.ParentId,
                    IncludeInMenu = model.IncludeInMenu,
                    IsPublished = model.IsPublished,
                    CreatedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid().ToString(),
                    ModifiedBy = currentUser.Id,
                    ModifiedOn = DateTime.UtcNow
                };

                await SaveCategoryImage(category, model);
                await _categoryService.Create(category);
                return Ok(new
                {
                    Status = "success",
                    Message = "Category added successfully.",
                    Data = category
                });
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(string id, [FromForm] CategoryRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var category = await _dbContext.Set<Category>().FirstOrDefaultAsync(x => x.Id == id);
                if (category == null)
                {
                    return NotFound();
                }

                category.Name = model.Name;
                category.Slug = model.Slug;
                category.MetaTitle = model.MetaTitle;
                category.MetaKeywords = model.MetaKeywords;
                category.MetaDescription = model.MetaDescription;
                category.Description = model.Description;
                category.DisplayOrder = model.DisplayOrder;
                category.ParentId = model.ParentId;
                category.IncludeInMenu = model.IncludeInMenu;
                category.IsPublished = model.IsPublished;

                if (!string.IsNullOrEmpty(category.ParentId) && await HaveCircularNesting(category.Id, category.ParentId))
                {
                    ModelState.AddModelError("ParentId", "Parent category cannot be itself children");
                    return BadRequest(ModelState);
                }

                await SaveCategoryImage(category, model);
                await _categoryService.Update(category);

                return Accepted();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(string id)
        {
            var category = _dbContext.Set<Category>().Include(x => x.Children).FirstOrDefault(x => x.Id == id);
            if (category == null)
            {
                return NotFound();
            }

            if (category.Children.Any(x => !x.IsDeleted))
            {
                return BadRequest(new { Error = "Please make sure this category contains no children" });
            }

            await _categoryService.Delete(category);
            return NoContent();
        }

        private async Task SaveCategoryImage(Category category, CategoryRequestModel model)
        {
            if (model.ThumbnailImage != null)
            {
                var fileName = await SaveFile(model.ThumbnailImage);
                if (category.ThumbnailImage != null)
                {
                    category.ThumbnailImage.FileName = fileName;
                }
                else
                {
                    category.ThumbnailImage = new Media
                    {
                        FileName = fileName,
                        CreatedBy = category.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid().ToString(),
                        ModifiedBy = category.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    };
                }
            }
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }

        private async Task<bool> HaveCircularNesting(string childId, string parentId)
        {
            var category = await _dbContext.Set<Category>().FirstOrDefaultAsync(x => x.Id == parentId);
            var parentCategoryId = category.ParentId;
            while (!string.IsNullOrEmpty(parentCategoryId))
            {
                if (parentCategoryId == childId)
                {
                    return true;
                }

                var parentCategory = await _dbContext.Set<Category>().FirstAsync(x => x.Id == parentCategoryId);
                parentCategoryId = parentCategory.ParentId;
            }

            return false;
        }
    }
}
