﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IWorkContext _workContext;
        private readonly IAccountService _accountService;

        public UsersController(IWorkContext workContext, IAccountService accountService)
        {
            _accountService = accountService;
            _workContext = workContext;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var user = await _accountService.Get()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            var model = new UserRequestModel
            {
                Id = user.Id,
                FullName = user.FullName,
                Email = user.EmailAddress,
                PhoneNumber = user.PhoneNumber
            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [Authorize]
        [HttpGet("logged-in")]
        public async Task<IActionResult> Get()
        {
            var currentUser = await _workContext.GetCurrentUser();

            var user = await _accountService.Get()
                .FirstOrDefaultAsync(x => x.Id == currentUser.Id);

            if (user == null)
            {
                return NotFound();
            }

            var model = new UserRequestModel
            {
                Id = user.Id,
                FullName = user.FullName,
                Email = user.EmailAddress,
                PhoneNumber = user.PhoneNumber,
            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Email,
                    EmailAddress = model.Email,
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    PasswordHash = AccountHelper.GeneratePasswordHash(model.Password),
                    ModifiedOn = DateTime.UtcNow,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedBy = model.Email,
                    CreatedBy = model.Email,
                    Id = Guid.NewGuid().ToString("N"),
                    
                };

                var createdAccount = await _accountService.CreateAsync(user);
                if (createdAccount != null)
                {
                    return CreatedAtAction(nameof(Get), new { id = user.Id }, null);
                }

                ModelState.AddModelError(string.Empty, "Sorry! User creation failed. Please try again");
            }

            return BadRequest(ModelState);
        }

    }
}
