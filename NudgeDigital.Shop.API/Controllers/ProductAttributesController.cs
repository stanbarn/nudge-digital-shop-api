﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductAttributesController : ControllerBase
    {
        private DbContext _dbContext;
        private readonly IWorkContext _workContext;

        public ProductAttributesController(IDatabaseContextFactory databaseContextFactory, IWorkContext workContext)
        {
            _workContext = workContext;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }


        [HttpGet]
        public IActionResult Get()
        {
            var attributes = _dbContext
                .Set<ProductAttribute>()
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    GroupName = x.Group.Name
                });

            return Ok(new
            {
                Status = "success",
                Data = attributes
            });
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var productAttribute = _dbContext
                .Set<ProductAttribute>().FirstOrDefault(x => x.Id == id);
            var model = new ProductAttributeRequestVm
            {
                Id = productAttribute.Id,
                Name = productAttribute.Name,
                GroupId = productAttribute.GroupId
            };

            return Ok(new
            {
                Status = "success",
                Data = model
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] ProductAttributeRequestVm model)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await _workContext.GetCurrentUser();

                var productAttribute = new ProductAttribute
                {
                    Id = Guid.NewGuid().ToString(),
                    CreatedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedBy = currentUser.Id,
                    ModifiedOn = DateTime.UtcNow,
                    Name = model.Name,
                    GroupId = model.GroupId
                };

                _dbContext.Add(productAttribute);
                _dbContext.SaveChanges();

                return Ok(new
                {
                    Status = "success",
                    Message = "Product Attribute added successfully.",
                    Data = productAttribute
                });
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(string id, [FromBody] ProductAttributeRequestVm model)
        {
            if (ModelState.IsValid)
            {
                var productAttribute = await _dbContext
                .Set<ProductAttribute>().FirstOrDefaultAsync(x => x.Id == id);
                productAttribute.Name = model.Name;
                productAttribute.GroupId = model.GroupId;

                await _dbContext.SaveChangesAsync();

                return Ok(new
                {
                    Status = "success"
                });
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(string id)
        {
            var productAttribute = _dbContext
                .Set<ProductAttribute>().FirstOrDefault(x => x.Id == id);
            if (productAttribute == null)
            {
                return NotFound();
            }

            _dbContext.Remove(productAttribute);
            return NoContent();
        }
    }
}
