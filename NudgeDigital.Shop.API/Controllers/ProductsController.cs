﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using NudgeDigital.Shop.API.Helpers;
using NudgeDigital.Shop.API.Models;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Enums;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Services;

namespace NudgeDigital.Shop.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;
        private readonly DbContext _dbContext;
        public ProductsController(IProductService productService, IMediaService mediaService, IWorkContext workContext, IDatabaseContextFactory databaseContextFactory)
        {
            _productService = productService;
            _mediaService = mediaService;
            _workContext = workContext;
            _dbContext = databaseContextFactory.GetDatabaseContext();
        }

        [HttpGet("{page}/{count}")]
        public IActionResult GetAll(int pageNumber, int pageSize)
        {
            var productQuery = _productService.Get()
                .Include(x => x.ThumbnailImage)
                .Include(x => x.Categories)
                .Where(x => x.IsPublished && !x.IsDeleted)
                .OrderByDescending(x => x.CreatedOn);

            var pagingParameterModel = new PagingParameterModel { PageSize = pageSize, PageNumber = pageNumber };

            // Page the collection   
            var pagedProducts = PagingHelper<object>.GeneratePaging(productQuery, pagingParameterModel, x => new
            {
                x.Id,
                x.Name,
                x.DisplayOrder,
                x.ShortDescription,
                x.Description,
                x.Price,
                x.OldPrice,
                x.SpecialPrice,
                x.SpecialPriceStart,
                x.SpecialPriceEnd,
                Categories = x.Categories.Select(y => y.CategoryId).ToList(),
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(x.ThumbnailImage)
            });


            return Ok(new 
            {
                pagedProducts.Collection,
                pagedProducts.TotalCount,
                pagedProducts.PageSize,
                pagedProducts.CurrentPage,
                pagedProducts.TotalPages,
                pagedProducts.PreviousPage,
                pagedProducts.NextPage
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var product = await _productService.Get()
                .Include(x => x.ThumbnailImage)
                .Include(x => x.Medias).ThenInclude(m => m.Media)
                .Include(x => x.OptionValues).ThenInclude(o => o.Option)
                .Include(x => x.AttributeValues).ThenInclude(a => a.Attribute).ThenInclude(g => g.Group)
                .Include(x => x.Categories)
                .FirstOrDefaultAsync(x => x.Id == id);

            var productVm = new ProductVm
            {
                Id = product.Id,
                Name = product.Name,
                Slug = product.Slug,
                MetaTitle = product.MetaTitle,
                MetaKeywords = product.MetaKeywords,
                MetaDescription = product.MetaDescription,
                Sku = product.Sku,
                Gtin = product.Gtin,
                ShortDescription = product.ShortDescription,
                Description = product.Description,
                Specification = product.Specification,
                OldPrice = product.OldPrice,
                Price = product.Price,
                SpecialPrice = product.SpecialPrice,
                SpecialPriceStart = product.SpecialPriceStart,
                SpecialPriceEnd = product.SpecialPriceEnd,
                IsFeatured = product.IsFeatured,
                IsPublished = product.IsPublished,
                IsCallForPricing = product.IsCallForPricing,
                IsAllowToOrder = product.IsAllowToOrder,
                CategoryIds = product.Categories.Select(x => x.CategoryId).ToList(),
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(product.ThumbnailImage),
                BrandId = product.BrandId,
                StockTrackingIsEnabled = product.StockTrackingIsEnabled
            };

            foreach (var productMedia in product.Medias.Where(x => x.Media.MediaType == MediaType.Image))
            {
                productVm.ProductImages.Add(new ProductMediaVm
                {
                    Id = productMedia?.Media?.Id,
                    MediaUrl = _mediaService.GetThumbnailUrl(productMedia.Media)
                });
            }

            productVm.Options = product.OptionValues.OrderBy(x => x.SortIndex).Select(x =>
                new ProductOptionVm
                {
                    Id = x.OptionId,
                    Name = x.Option.Name,
                    DisplayType = x.DisplayType,
                    Values = JsonConvert.DeserializeObject<IList<ProductOptionValueModel>>(x.Value)

                }).ToList();

            productVm.Attributes = product.AttributeValues.Select(x => new ProductAttributeVm 
            {
                AttributeValueId = x.Id,
                Id = x.AttributeId,
                Name = x.Attribute.Name,
                GroupName = x.Attribute.Group.Name,
                Value = x.Value

            }).ToList();

            return Ok(new
            {
                Message = StatusCodes.Status200OK,
                Data = productVm
            });
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromForm] ProductRequestModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var currentUser = await _workContext.GetCurrentUser();

            if (await _productService.Get().SingleOrDefaultAsync(x => x.Name == model.Product.Name) != null)
            {
                ModelState.AddModelError("", $"Product With Name {model.Product.Name} Already Exists");
                return BadRequest(ModelState);
            }

            var product = new Product
            {
                Name = model.Product.Name,
                Slug = model.Product.Slug,
                MetaTitle = model.Product.MetaTitle,
                MetaKeywords = model.Product.MetaKeywords,
                MetaDescription = model.Product.MetaDescription,
                Sku = model.Product.Sku,
                Gtin = model.Product.Gtin,
                ShortDescription = model.Product.ShortDescription,
                Description = model.Product.Description,
                Specification = model.Product.Specification,
                Price = model.Product.Price,
                OldPrice = model.Product.OldPrice,
                SpecialPrice = model.Product.SpecialPrice,
                SpecialPriceStart = model.Product.SpecialPriceStart,
                SpecialPriceEnd = model.Product.SpecialPriceEnd,
                IsPublished = model.Product.IsPublished,
                IsFeatured = model.Product.IsFeatured,
                IsCallForPricing = model.Product.IsCallForPricing,
                IsAllowToOrder = model.Product.IsAllowToOrder,
                BrandId = model.Product.BrandId,
                StockTrackingIsEnabled = model.Product.StockTrackingIsEnabled,
                IsVisibleIndividually = true,
                CreatedBy = currentUser.Id,
                ModifiedBy = currentUser.Id,
                Id = Guid.NewGuid().ToString(),
                ModifiedOn = DateTime.UtcNow,
                CreatedOn = DateTime.UtcNow,

            };

            var optionIndex = 0;
            foreach (var option in model.Product.Options)
            {
                product.AddOptionValue(new ProductOptionValue
                {
                    OptionId = option.Id,
                    DisplayType = option.DisplayType,
                    Value = JsonConvert.SerializeObject(option.Values),
                    SortIndex = optionIndex,
                    CreatedBy = currentUser.Id,
                    ModifiedBy = currentUser.Id,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid().ToString(),
                    ModifiedOn = DateTime.UtcNow,
                    ProductId = product.Id
                });

                optionIndex++;
            }

            foreach (var attribute in model.Product.Attributes)
            {
                var attributeValue = new ProductAttributeValue
                {
                    AttributeId = attribute.Id,
                    Value = attribute.Value
                };

                product.AddAttributeValue(attributeValue);
            }

            foreach (var categoryId in model.Product.CategoryIds)
            {
                var productCategory = new ProductCategory
                {
                    CategoryId = categoryId
                };
                product.AddCategory(productCategory);
            }

            await SaveProductMedias(model, product);

            await _productService.Create(product);

            return Ok(new
            {
                response = "success",
                message = "product added successfully.",
                id = product.Id
            });
        }

        private void AddOrDeleteCategories(List<string> CategoryIds, Product product)
        {
            foreach (var categoryId in CategoryIds)
            {
                if (product.Categories.Any(x => x.CategoryId == categoryId))
                {
                    continue;
                }

                var productCategory = new ProductCategory
                {
                    CategoryId = categoryId,
                    ProductId = product.Id,
                };
                product.AddCategory(productCategory);
            }

            var deletedProductCategories =
                product.Categories.Where(productCategory => !CategoryIds.Contains(productCategory.CategoryId))
                    .ToList();

            foreach (var deletedProductCategory in deletedProductCategories)
            {
                deletedProductCategory.Product = null;
                product.Categories.Remove(deletedProductCategory);
                _dbContext.Set<ProductCategory>().Remove(deletedProductCategory);
            }
        }

        private void AddOrDeleteProductOption(List<ProductOptionVm> Options, Product product)
        {
            var optionIndex = 0;
            foreach (var optionVm in Options)
            {
                var optionValue = product.OptionValues.FirstOrDefault(x => x.OptionId == optionVm.Id);
                if (optionValue == null)
                {
                    product.AddOptionValue(new ProductOptionValue
                    {
                        OptionId = optionVm.Id,
                        DisplayType = optionVm.DisplayType,
                        Value = JsonConvert.SerializeObject(optionVm.Values),
                        SortIndex = optionIndex,
                        Id = Guid.NewGuid().ToString(),
                        CreatedBy = product.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedBy = product.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    });
                }
                else
                {
                    optionValue.Value = JsonConvert.SerializeObject(optionVm.Values);
                    optionValue.DisplayType = optionVm.DisplayType;
                    optionValue.SortIndex = optionIndex;
                }

                optionIndex++;
            }

            var deletedProductOptionValues = product.OptionValues.Where(x => Options.All(vm => vm.Id != x.OptionId)).ToList();

            foreach (var productOptionValue in deletedProductOptionValues)
            {
                product.OptionValues.Remove(productOptionValue);
                _dbContext.Set<ProductOptionValue>().Remove(productOptionValue);
            }
        }

        private void AddOrDeleteProductAttribute(List<ProductAttributeVm> Attributes, Product product)
        {
            foreach (var productAttributeVm in Attributes)
            {
                var productAttrValue =
                    product.AttributeValues.FirstOrDefault(x => x.AttributeId == productAttributeVm.Id);

                if (productAttrValue == null)
                {
                    productAttrValue = new ProductAttributeValue
                    {
                        AttributeId = productAttributeVm.Id,
                        Value = productAttributeVm.Value,
                        CreatedBy = product.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid().ToString(),
                        ModifiedBy = product.ModifiedBy,
                        ModifiedOn = DateTime.UtcNow,
                        ProductId = product.Id
                    };

                    product.AddAttributeValue(productAttrValue);
                }
                else
                    productAttrValue.Value = productAttributeVm.Value;
            }

            var deletedAttrValues =
                product.AttributeValues.Where(attrValue => Attributes.All(x => x.Id != attrValue.AttributeId))
                    .ToList();

            foreach (var deletedAttrValue in deletedAttrValues)
            {
                deletedAttrValue.Product = null;
                product.AttributeValues.Remove(deletedAttrValue);
                _dbContext.Set<ProductAttributeValue>().Remove(deletedAttrValue);
            }
        }

        private async Task SaveProductMedias(ProductRequestModel model, Product product)
        {
            if (model.ThumbnailImage != null)
            {
                var fileName = await SaveFile(model.ThumbnailImage);
                if (product.ThumbnailImage != null)
                    product.ThumbnailImage.FileName = fileName;

                else
                    product.ThumbnailImage = new Media 
                    { 
                        FileName = fileName, 
                        Id = Guid.NewGuid().ToString() ,
                        CreatedBy = product.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedBy = product.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    };
            }

            // Currently model binder cannot map the collection of file productImages[0], productImages[1]
            foreach (var file in Request.Form.Files)
            {
                if (file.ContentDisposition.Contains("productImages"))
                    model.ProductImages.Add(file);

                else if (file.ContentDisposition.Contains("productDocuments"))
                    model.ProductDocuments.Add(file);
            }

            foreach (var file in model.ProductImages)
            {
                var fileName = await SaveFile(file);
                var productMedia = new ProductMedia
                {
                    Product = product,
                    Media = new Media 
                    { 
                        FileName = fileName, 
                        MediaType = MediaType.Image, 
                        Id = Guid.NewGuid().ToString(),
                        CreatedBy = product.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedBy = product.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    },
                };
                product.AddMedia(productMedia);
            }

            foreach (var file in model.ProductDocuments)
            {
                var fileName = await SaveFile(file);
                var productMedia = new ProductMedia
                {
                    Product = product,
                    Media = new Media 
                    { FileName = fileName, 
                        MediaType = MediaType.File, 
                        Caption = file.FileName, 
                        Id = Guid.NewGuid().ToString() ,
                        CreatedBy = product.CreatedBy,
                        CreatedOn = DateTime.UtcNow,
                        ModifiedBy = product.CreatedBy,
                        ModifiedOn = DateTime.UtcNow
                    }
                };
                product.AddMedia(productMedia);
            }
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }
    }
}
