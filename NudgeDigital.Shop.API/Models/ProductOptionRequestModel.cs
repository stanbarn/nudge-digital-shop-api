﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductOptionRequestModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
