﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductOptionVm
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string DisplayType { get; set; }

        public IList<ProductOptionValueModel> Values { get; set; } = new List<ProductOptionValueModel>();
    }
}
