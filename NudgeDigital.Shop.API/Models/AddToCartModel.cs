﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class AddToCartModel
    {
        public string ProductId { get; set; }

        public int Quantity { get; set; }
    }
}
