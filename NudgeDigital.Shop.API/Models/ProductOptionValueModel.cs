﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductOptionValueModel
    {
        public string Key { get; set; }

        public string Display { get; set; }
    }
}
