﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class BrandRequestModel
    {
        public BrandRequestModel()
        {
            IsPublished = true;
        }

        public string Id { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string Slug { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string Name { get; set; }

        public IFormFile ThumbnailImage { get; set; }

        public bool IsPublished { get; set; }

        public IList<string> CategoryIds { get; set; }
    }
}
