﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class TokenModel
    {
        [Required(ErrorMessage = "The {0} field is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string Password { get; set; }
    }
}
