﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductMediaVm
    {
        public string Id { get; set; }

        public string Caption { get; set; }

        public string MediaUrl { get; set; }
    }
}
