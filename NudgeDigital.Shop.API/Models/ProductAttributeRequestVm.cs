﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductAttributeRequestVm
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string GroupId { get; set; }
    }
}
