﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class TokenRefreshModel
    {
        [Required(ErrorMessage = "The {0} field is required.")]
        public string Token { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string RefreshToken { get; set; }
    }
}
