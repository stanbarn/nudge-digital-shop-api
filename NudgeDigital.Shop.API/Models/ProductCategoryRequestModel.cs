﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.API.Models
{
    public class ProductCategoryRequestModel
    {
        public string Id { get; set; }

        public bool IsFeaturedProduct { get; set; }

        public int DisplayOrder { get; set; }
    }
}
