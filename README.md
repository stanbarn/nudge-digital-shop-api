## Nudge Digital Shop API
Nudge Digital Shop API is an ecommerce API that provides all required functionality for the front end.


## Tech/framework used

### Built with
- [.NETCore 3.1](https://dotnet.microsoft.com/download/dotnet/3.1)
- [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-2019)
- [EF Core](https://docs.microsoft.com/en-us/ef/core/)

## Features
- Authenticate User Using JWT token based authentication
- Ability to Add, Update, and Get Products to the Catalog
- Add, Update and Get Product Options (Configurations).
- Add, Update and Get Categories
- Add, Update, and Get Brands.
- Add items to the Shopping Cart
- Add, Update and Get Product Attributes
- Add, Update and Get Product Attribute Groups.


## Installation

    Clone this repository
    Build the solution using Visual Studio, or on the command line with dotnet build.
    Run the project. The API will start up on https://localhost:44375/ or http://localhost:51727
    Use an HTTP client like Postman or Fiddler to Try out the API https://localhost:44375/.

## Security
If you believe you have found a security issue in This API, please share it with us privately via email (stanleybarna@gmail.com). Reporting it via this channel helps minimize risk to projects built on top of this API


## License
Copyright (c) Nudge Digital. All rights reserved.

MIT � [Stanley Barnabas Nagwere]()