﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Settings
{
    public static class AppSettings
    {
        private static IConfiguration _configuration => new ConfigurationBuilder()
            .SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: false)
            //.AddJsonFile($"appsettings.Development.json", optional: false, reloadOnChange: false)
            .Build();

        //Database connection string
        public static string DatabaseConnectionString
            => _configuration.GetConnectionString("DatabaseConnectionString");

        public static string AuthSecretKey
            => _configuration["Authentication:JWT:SecretKey"];

        public static string AuthIssuer
            => _configuration["Authentication:JWT:Issuer"];

        public static string AuthDurationInMinutes
            => _configuration["Authentication:JWT:TokenDurationInMinutes"];

        public static string AzureBlobEndPoint
            => _configuration["Azure:Blob:EndPoint"];

        public static string AzureBlobConnectionString
            => _configuration["Azure:Blob:StorageConnectionString"];

        public static string AzureBlobContainerName
            => _configuration["Azure:Blob:ContainerName"];
    }
}
