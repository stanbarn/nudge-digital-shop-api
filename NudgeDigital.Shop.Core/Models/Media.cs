﻿using NudgeDigital.Shop.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class Media : ModifiableEntity
    {
        [StringLength(450)]
        public string Caption { get; set; }

        public int FileSize { get; set; }

        [StringLength(450)]
        public string FileName { get; set; }

        public MediaType MediaType { get; set; }

        public ICollection<ProductMedia> ProductMedias { get; protected set; } = new List<ProductMedia>();

        public ICollection<BrandMedia> BrandMedia { get; set; } = new List<BrandMedia>();
    }
}
