﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class Cart : ModifiableEntity
    {
        public string CustomerId { get; set; }

        public User Customer { get; set; }

        public DateTimeOffset LatestUpdatedOn { get; set; }

        public bool IsActive { get; set; }

        public bool LockedOnCheckout { get; set; }

        [StringLength(450)]
        public string ShippingMethod { get; set; }

        public decimal? ShippingAmount { get; set; }

        public IList<CartItem> Items { get; set; } = new List<CartItem>();

        /// <summary>
        /// Json serialized of shipping form
        /// </summary>
        public string ShippingData { get; set; }

        [StringLength(1000)]
        public string OrderNote { get; set; }
    }
}
