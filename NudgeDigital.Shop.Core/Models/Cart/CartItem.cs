﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class CartItem : ModifiableEntity
    {
        public string ProductId { get; set; }

        public Product Product { get; set; }

        public int Quantity { get; set; }

        public string CartId { get; set; }

        public Cart Cart { get; set; }
    }
}
