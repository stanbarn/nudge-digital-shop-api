﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class CartItemVm
    {
        public string Id { get; set; }

        public string ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductImage { get; set; }

        public decimal ProductPrice { get; set; }

        public int ProductStockQuantity { get; set; }

        public bool ProductStockTrackingIsEnabled { get; set; }

        public bool IsProductAvailabeToOrder { get; set; }

        public int Quantity { get; set; }

        public decimal Total => Quantity * ProductPrice;

        public decimal Price;
    }
}
