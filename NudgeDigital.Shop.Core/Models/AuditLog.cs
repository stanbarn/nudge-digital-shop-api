﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    /// <summary>
    /// Database operation record
    /// </summary>
    public class AuditLog
    {
        [Key, StringLength(64)]
        public string AuditLogId { get; set; }

        [StringLength(255)]
        public string UserId { get; set; }

        [StringLength(255)]
        public string RecordId { get; set; }

        [StringLength(255)]
        public string TableName { get; set; }

        public string OriginalValue { get; set; }

        [StringLength(255)]
        public string ColumnName { get; set; }

        public string NewValue { get; set; }

        [StringLength(50)]
        public string EventType { get; set; }

        [Required]
        public DateTime CreatedOnDateUTC { get; set; }
    }
}
