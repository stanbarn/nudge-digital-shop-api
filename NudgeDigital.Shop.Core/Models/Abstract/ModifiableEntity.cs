﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    /// <summary>
    /// Base class for all modifiable entities
    /// </summary>
    public abstract class ModifiableEntity : EntityBase
    {
        public ModifiableEntity()
        {
            ModifiedOn = DateTime.UtcNow;
        }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }
    }
}
