﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class ProductCategory
    {
        public string CategoryId { get; set; }
        public Category Category { get; set; }

        public string ProductId { get; set; }
        public Product Product { get; set; }
    }
}
