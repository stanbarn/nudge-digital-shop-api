﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class Brand : ModifiableEntity
    {
        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string Name { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string Slug { get; set; }

        public string Description { get; set; }

        public Media ThumbnailImage { get; set; }

        public bool IsPublished { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<BrandCategory> BrandCategories { get; set; } = new List<BrandCategory>();

        public ICollection<BrandMedia> BrandMedia { get; set; } = new List<BrandMedia>();
    }
}
