﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class ProductOptionValue : ModifiableEntity
    {
        public string OptionId { get; set; }

        public ProductOption Option { get; set; }

        public string ProductId { get; set; }

        public Product Product { get; set; }

        [StringLength(450)]
        public string Value { get; set; }

        [StringLength(450)]
        public string DisplayType { get; set; }

        public int SortIndex { get; set; }
    }
}
