﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class ProductAttribute : ModifiableEntity
    {
        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string Name { get; set; }

        public string GroupId { get; set; }

        public ProductAttributeGroup Group { get; set; }
    }
}
