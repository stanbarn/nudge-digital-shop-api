﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class ProductAttributeValue : ModifiableEntity
    {
        public string AttributeId { get; set; }
        public ProductAttribute Attribute { get; set; }

        public string ProductId { get; set; }
        public Product Product { get; set; }

        public string Value { get; set; }
    }
}
