﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class ProductOptionCombination : ModifiableEntity
    {
        public string ProductId { get; set; }

        public Product Product { get; set; }

        public string OptionId { get; set; }

        public ProductOption Option { get; set; }

        [StringLength(450)]
        public string Value { get; set; }

        public int SortIndex { get; set; }
    }
}
