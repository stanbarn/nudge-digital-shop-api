﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class BrandCategory
    {
        public bool IsFeaturedBrand { get; set; }

        public string CategoryId { get; set; }
        public Category Category { get; set; }

        public string BrandId { get; set; }
        public Brand Brand { get; set; }
    }
}
