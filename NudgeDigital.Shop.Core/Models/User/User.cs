﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class User : ModifiableEntity
    {
        public User()
        {
            CreatedOn = DateTime.UtcNow;
            ModifiedOn = DateTime.UtcNow;
        }

        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string FullName { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string PasswordHash { get; set; }

        public bool IsDeleted { get; set; }

        public IList<UserAddress> UserAddresses { get; set; } = new List<UserAddress>();

        public UserAddress DefaultShippingAddress { get; set; }

        public string DefaultShippingAddressId { get; set; }

        public UserAddress DefaultBillingAddress { get; set; }

        public string DefaultBillingAddressId { get; set; }

        [StringLength(450)]
        public string RefreshTokenHash { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
    }
}
