﻿using NudgeDigital.Shop.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Models
{
    public class UserAddress : ModifiableEntity
    {
        public string UserId { get; set; }

        public User User { get; set; }

        public string AddressId { get; set; }

        public Address Address { get; set; }

        public AddressType AddressType { get; set; }

        public DateTimeOffset? LastUsedOn { get; set; }
    }
}
