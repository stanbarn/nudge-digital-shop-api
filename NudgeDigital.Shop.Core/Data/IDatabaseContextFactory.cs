﻿using Microsoft.EntityFrameworkCore;

namespace NudgeDigital.Shop.Core.Data
{
    public interface IDatabaseContextFactory
    {
        DbContext GetDatabaseContext();
    }
}
