﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Enums
{
    public enum MessageLevel
    {
        ERROR,
        INFO,
        WARNING
    }
}
