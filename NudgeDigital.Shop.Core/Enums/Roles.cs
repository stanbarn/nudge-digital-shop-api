﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Core.Enums
{
    public enum Roles
    {
        GUEST = 0,
        CUSTOMER = 1,
        ADMINISTRATOR = 2,
    }
}
