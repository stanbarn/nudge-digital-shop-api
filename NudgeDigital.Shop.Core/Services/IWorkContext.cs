﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface IWorkContext
    {
        Task<User> GetCurrentUser();
    }
}
