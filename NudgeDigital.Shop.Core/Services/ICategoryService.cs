﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface ICategoryService
    {
        IQueryable<Category> Get();

        Task<Category> Get(string id);

        Task<Category> Create(Category category);

        Task<Category> Update(Category category);

        Task<Category> Delete(Category category);
    }
}
