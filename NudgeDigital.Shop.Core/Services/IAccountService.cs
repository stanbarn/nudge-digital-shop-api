﻿using System.Linq;
using NudgeDigital.Shop.Core.Models;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface IAccountService
    {
        IQueryable<User> Get();

        Task<User> GetAsync(string AccountId);

        Task<User> UpdateAsync(User Account);


        Task<User> CreateAsync(User account);


        Task<User> GetByEmailAsync(string email);

        Task<User> ActivateAccountAsync(string email, string auth);

        Task<User> AuthenticateAsync(string username, string password);

        Task<bool> IsEmailExistsAsync(string emailAddress);

        Task<User> DeleteAsync(string Id);
    }
}
