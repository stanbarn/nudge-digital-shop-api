﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface IBrandService
    {
        IQueryable<Brand> Get();

        Task<Brand> Get(string id);

        Task<Brand> Create(Brand brand);

        Task<Brand> Update(Brand brand);

        Task<Brand> Delete(string id);
    }
}
