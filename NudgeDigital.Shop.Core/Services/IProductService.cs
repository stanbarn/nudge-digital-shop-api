﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface IProductService
    {
        IQueryable<Product> Get();

        Task<Product> Get(string id);

        Task<Product> Create(Product product);

        Task Update(Product product);

        Task Delete(Product product);
    }
}
