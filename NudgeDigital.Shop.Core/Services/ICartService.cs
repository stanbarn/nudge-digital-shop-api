﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface ICartService
    {
        Task<Result> AddToCart(string customerId, string createdById, string productId, int quantity);

        Task<CartVm> GetActiveCartDetails(string customerId, string createdById);

        IQueryable<Cart> Get();

        Task<Cart> GetActiveCart(string customerId, string createdById);


        Task MigrateCart(string fromUserId, string toUserId);
    }
}
