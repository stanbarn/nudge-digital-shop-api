﻿using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Core.Services
{
    public interface IEntityService
    {
        Task<string> ToSafeSlug(string slug, string entityId, string entityTypeId);

        Task<Entity> Get(string entityId, string entityTypeId);

        Task Add(string name, string slug, string entityId, string entityTypeId);

        Task Update(string newName, string newSlug, string entityId, string entityTypeId);

        Task Remove(string entityId, string entityTypeId);
    }
}
