﻿using Newtonsoft.Json;
using NudgeDigital.Shop.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NudgeDigital.Shop.Core.Exceptions
{
    [Serializable]
    public class ProductException : Exception
    {
        public ProductException() { }
        public ProductException(string message) : base(message) { }
        public ProductException(SimpleMessage simpleMessage) : base(JsonConvert.SerializeObject(simpleMessage)) { }
        public ProductException(string message, Exception inner) : base(message, inner) { }
        public ProductException(SimpleMessage simpleMessage, Exception inner) : base(JsonConvert.SerializeObject(simpleMessage), inner) { }
        protected ProductException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
