﻿using Newtonsoft.Json;
using NudgeDigital.Shop.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NudgeDigital.Shop.Core.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException() { }
        public ServiceException(string message) : base(message) { }
        public ServiceException(SimpleMessage simpleMessage) : base(JsonConvert.SerializeObject(simpleMessage)) { }
        public ServiceException(string message, Exception inner) : base(message, inner) { }
        public ServiceException(SimpleMessage simpleMessage, Exception inner) : base(JsonConvert.SerializeObject(simpleMessage), inner) { }
        protected ServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
