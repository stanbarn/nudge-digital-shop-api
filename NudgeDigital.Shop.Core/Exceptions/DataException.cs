﻿using Newtonsoft.Json;
using NudgeDigital.Shop.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NudgeDigital.Shop.Core.Exceptions
{
    [Serializable]
    public class DataException : Exception
    {
        public DataException() { }
        public DataException(string message) : base(message) { }
        public DataException(SimpleMessage simpleMessage) : base(JsonConvert.SerializeObject(simpleMessage)) { }
        public DataException(string message, Exception inner) : base(message, inner) { }
        public DataException(SimpleMessage simpleMessage, Exception inner) : base(JsonConvert.SerializeObject(simpleMessage), inner) { }
        protected DataException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}
