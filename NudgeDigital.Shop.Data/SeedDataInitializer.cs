﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NudgeDigital.Shop.Data
{
    public static class SeedDataInitializer
    {
        public static void SeedData(ModelBuilder builder)
        {
            builder.Entity<User>().HasData(
                new User { Id = "2L", CreatedOn = DateTime.UtcNow, EmailAddress = "system@nudgedigital.com", EmailConfirmed = false, FullName = "System User", IsDeleted = false, PasswordHash = "AQAAAAEAACcQAAAAEAEqSCV8Bpg69irmeg8N86U503jGEAYf75fBuzvL00/mr/FGEsiUqfR0rWBbBUwqtw==", PhoneNumberConfirmed = false,  ModifiedOn = DateTime.UtcNow, UserName = "system@nudgedigital.com", CreatedBy = "10L", ModifiedBy = "10L", PhoneNumber = "256702173540" },

                new User { Id = "10L", CreatedOn = DateTime.UtcNow, EmailAddress = "admin@nudgedigital.com", EmailConfirmed = false, FullName = "Shop Admin", IsDeleted = false, PasswordHash = "AQAAAAEAACcQAAAAEAEqSCV8Bpg69irmeg8N86U503jGEAYf75fBuzvL00/mr/FGEsiUqfR0rWBbBUwqtw==", PhoneNumberConfirmed = false, ModifiedOn = DateTime.UtcNow, UserName = "admin@nudgedigital.com", CreatedBy = "10L", ModifiedBy = "10L", PhoneNumber = "256779705618" }
            );

            builder.Entity<Country>().HasData(
                new Country { Id = "UG", CountryCode = "UG", Name = "Uganda", IsBillingEnabled = true, IsShippingEnabled = true, IsCityEnabled = false, IsZipCodeEnabled = false, IsDistrictEnabled = true, CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" },

                new Country { Id = "US", CountryCode = "USA", Name = "United States", IsBillingEnabled = true, IsShippingEnabled = true, IsCityEnabled = true, IsZipCodeEnabled = true, IsDistrictEnabled = false, CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" }
            );

            builder.Entity<StateOrProvince>().HasData(
                new StateOrProvince { Id = "12eb3ed8210c3-24b0-4823-a744-80078cf", CountryId = "UG", Name = "Central",  CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" },
                new StateOrProvince { Id = "f12eb5ed8210c3-24b0-4823-a744-80078c", CountryId = "US", Name = "Washington", Code = "WA", CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" }
            );

            builder.Entity<Address>().HasData(
                new Address { Id = Guid.NewGuid().ToString(), AddressLine1 = "Kira town, Kampala", ContactName = "Stanley Barnabas Nagwere", CountryId = "UG", CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" }
            );

            builder.Entity<ProductOption>().HasData(
                new ProductOption { Id = Guid.NewGuid().ToString(), Name = "Color", CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" },
                new ProductOption { Id = Guid.NewGuid().ToString(), Name = "Size", CreatedBy = "10L", CreatedOn = DateTime.UtcNow, ModifiedBy = "10L" }
            );

            builder.Entity<EntityType>().HasData(
                new EntityType("Category") { IsMenuable = true },

                new EntityType("Brand") { IsMenuable = true },

                new EntityType("Product") { IsMenuable = false }

            );
        }
    }
}
