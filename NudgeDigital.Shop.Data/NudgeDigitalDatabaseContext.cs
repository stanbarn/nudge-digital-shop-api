﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using NudgeDigital.Shop.Core.Exceptions;
using NudgeDigital.Shop.Core.Models;
using NudgeDigital.Shop.Core.Models.IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NudgeDigital.Shop.Data
{
    public class NudgeDigitalDatabaseContext : DbContext
    {
        public NudgeDigitalDatabaseContext(DbContextOptions<NudgeDigitalDatabaseContext> options)
            : base(options)
        {
        }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<StateOrProvince> StatesOrProvinces { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public DbSet<Cart> ShoppingCart { get; set; }
        public DbSet<CartItem> ShoppingCartItems { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductAttribute> ProductAttributes { get; set; }
        public DbSet<ProductAttributeGroup> ProductAttributeGroups { get; set; }
        public DbSet<ProductAttributeValue> ProductAttributeValues { get; set; }
        public DbSet<ProductOption> ProductOptions { get; set; }
        public DbSet<ProductOptionCombination> ProductOptionCombinations { get; set; }
        public DbSet<ProductOptionValue> ProductOptionValues { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AuditLog> AuditLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //configure unique indices
            modelBuilder.Entity<User>()
                .HasIndex(x => x.EmailAddress)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(x => x.UserName)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(x => x.PhoneNumber)
                .IsUnique(true);

            modelBuilder.Entity<ProductOption>()
                .HasIndex(x => x.Name)
                .IsUnique(true);


            modelBuilder.Entity<Product>()
                .HasIndex(x => x.Name)
                .IsUnique(true);

            modelBuilder.Entity<ProductAttributeGroup>()
                .HasIndex(x => x.Name)
                .IsUnique(true);

            modelBuilder.Entity<Brand>()
                .HasIndex(x => x.Name)
                .IsUnique(true);

            modelBuilder.Entity<Category>()
                .HasIndex(x => x.Name)
                .IsUnique(true);

            //Core entities

            modelBuilder.Entity<Entity>().ToTable("Entities");

            modelBuilder.Entity<EntityType>().ToTable("EntityTypes");

            modelBuilder.Entity<User>(u =>
            {
                u.HasOne(x => x.DefaultShippingAddress)
                   .WithMany()
                   .HasForeignKey(x => x.DefaultShippingAddressId)
                   .OnDelete(DeleteBehavior.Restrict);

                u.HasOne(x => x.DefaultBillingAddress)
                    .WithMany()
                    .HasForeignKey(x => x.DefaultBillingAddressId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<UserAddress>()
                .HasOne(x => x.User)
                .WithMany(a => a.UserAddresses)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Address>(x =>
            {
                x.HasOne(d => d.Country)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict);
            });

            //Add many to many relationship for brand and category
            modelBuilder.Entity<BrandCategory>()
                .HasKey(k => new { k.CategoryId, k.BrandId });

            modelBuilder.Entity<BrandCategory>()
                .HasOne(c => c.Brand)
                .WithMany(o => o.BrandCategories);

            modelBuilder.Entity<BrandCategory>()
                .HasOne(c => c.Category)
                .WithMany(o => o.BrandCategories);

            //Add many to many relationship for brand and Media
            modelBuilder.Entity<BrandMedia>()
                .HasKey(k => new { k.MediaId, k.BrandId });
            modelBuilder.Entity<BrandMedia>()
                .HasOne(c => c.Brand)
                .WithMany(o => o.BrandMedia);

            modelBuilder.Entity<BrandMedia>()
                .HasOne(c => c.Media)
                .WithMany(o => o.BrandMedia);

            //Add many to many relationship for Product and category
            modelBuilder.Entity<ProductCategory>()
                .HasKey(k => new { k.CategoryId, k.ProductId });
            modelBuilder.Entity<ProductCategory>()
                .HasOne(c => c.Product)
                .WithMany(o => o.Categories);

            modelBuilder.Entity<ProductCategory>()
                .HasOne(c => c.Category)
                .WithMany(o => o.ProductCategories);

            //Add many to many relationship for Product and Media
            modelBuilder.Entity<ProductMedia>()
                .HasKey(k => new { k.MediaId, k.ProductId });
            modelBuilder.Entity<ProductMedia>()
                .HasOne(c => c.Product)
                .WithMany(o => o.Medias);

            modelBuilder.Entity<ProductMedia>()
                .HasOne(c => c.Media)
                .WithMany(o => o.ProductMedias);

            //Disable cascade delete for all foreign Key relationships
            var cascadeForeignKeys = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(foreinKey => !foreinKey.IsOwnership && foreinKey.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var foreinKey in cascadeForeignKeys)
            {
                foreinKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);

            // seed data
            SeedDataInitializer.SeedData(modelBuilder);
        }

        public override int SaveChanges()
        {
            try
            {
                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them
                    foreach (AuditLog x in GetAuditRecordsForChange(ent))
                    {
                        this.AuditLogs.Add(x);
                    }
                }

                return base.SaveChanges();
            }
            catch (DbUpdateConcurrencyException exception)
            {
                throw new DataException("DB Error: " + exception.InnerException.Message, exception.InnerException);
            }
            catch (System.Data.DataException exception)
            {
                throw new DataException(exception.Message, exception.InnerException);
            }
            catch (DataException exception)
            {
                throw new DataException(exception.Message, exception);
            }
            catch (Exception exception)
            {
                throw new DataException("DB Error: " + exception.Message, exception.InnerException);
            }
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them
                    foreach (AuditLog x in GetAuditRecordsForChange(ent))
                    {
                        this.AuditLogs.Add(x);
                    }
                }

                return await base.SaveChangesAsync(true, cancellationToken);
            }
            catch (DbUpdateConcurrencyException exception)
            {
                throw new DataException("DB Error: " + exception.InnerException.Message, exception.InnerException);
            }
            catch (System.Data.DataException exception)
            {
                throw new DataException("DB Error: " + exception.InnerException.InnerException.Message, exception.InnerException);
            }
            catch (Exception exception)
            {
                throw new DataException("DB Error: " + exception.Message, exception.InnerException);
            }
        }

        private List<AuditLog> GetAuditRecordsForChange(EntityEntry dbEntry, string userId = "user")
        {
            try
            {
                var result = new List<AuditLog>();

                DateTime changeTime = DateTime.UtcNow;

                // Get the Table() attribute, if one exists
                TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() as TableAttribute;

                // Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
                string tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;

                // Get primary key value (If you have more than one key column, this will need to be adjusted)
                var keyNames = dbEntry.Entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();

                string keyName = "";
                if (keyNames.Count > 0)
                {
                    keyName = keyNames[0].Name;
                }


                if (dbEntry.State == EntityState.Added)
                {
                    // For Inserts, just add the whole record
                    // If the entity implements IDescribableEntity, use the description from Describe(), otherwise use ToString()


                }

                if (dbEntry.State == EntityState.Deleted)
                {
                    // Same with deletes, do the whole record, and use either the description from Describe() or ToString()
                    result.Add(new AuditLog()
                    {
                        AuditLogId = Guid.NewGuid().ToString("N"),
                        UserId = userId,
                        CreatedOnDateUTC = changeTime,
                        EventType = "Deleted", // Deleted
                        TableName = tableName,
                        RecordId = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                        ColumnName = "*ALL",
                        NewValue = (dbEntry.OriginalValues.ToObject() is IDescribableEntity) ? (dbEntry.OriginalValues.ToObject() as IDescribableEntity).Describe() : dbEntry.OriginalValues.ToObject().ToString()
                    });
                }

                if (dbEntry.State == EntityState.Modified)
                {
                }

                return result;
            }
            catch (DbUpdateConcurrencyException exception)
            {
                throw new DataException("DB Error: " + exception.InnerException.Message, exception.InnerException);
            }
            catch (System.Data.DataException exception)
            {
                throw new DataException("DB Error: " + exception.InnerException.InnerException.Message, exception.InnerException);
            }
            catch (Exception exception)
            {
                throw new DataException("DB Error: " + exception.Message, exception.InnerException);
            }
        }
    }
}
