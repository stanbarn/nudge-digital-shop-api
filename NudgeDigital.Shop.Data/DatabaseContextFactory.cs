﻿using Microsoft.EntityFrameworkCore;
using NudgeDigital.Shop.Core.Data;
using NudgeDigital.Shop.Core.Settings;

namespace NudgeDigital.Shop.Data
{
    public class DatabaseContextFactory : IDatabaseContextFactory
    {
        public DbContext GetDatabaseContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<NudgeDigitalDatabaseContext>();
            optionsBuilder.UseSqlServer(AppSettings.DatabaseConnectionString, builder =>
            {
                builder.EnableRetryOnFailure();
            });
            optionsBuilder.EnableSensitiveDataLogging(true);

            // Ensure that the SQL database and sechema is created!{Remember to remove this when deploying to production}
            var context = new NudgeDigitalDatabaseContext(optionsBuilder.Options);
            context.Database.EnsureCreated();

            return context;
        }
    }
}
